package ar.com.leadprogrammer.framework.configuration;

import java.util.Arrays;

import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import ar.com.leadprogrammer.framework.rest.autentication.filter.SecurityContextFilter;

@SpringBootApplication
@ComponentScan(value="ar.com.leadprogrammer.framework", includeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, value = Provider.class))
public class MicroserviceStartUp {

	
	@Configuration
    public static class JerseyConfig extends ResourceConfig {

        public JerseyConfig() {
           this.register(SecurityContextFilter.class);
           this.register(RolesAllowedDynamicFeature.class);

        }
    }

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(MicroserviceStartUp.class, args);

        System.out.println("Let's inspect the beans provided by Spring Boot:");

        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        for (String beanName : beanNames) {
            System.out.println(beanName);
        }
    }

}