package ar.com.leadprogrammer.framework.model;

/**
 * User: porter
 * Date: 03/04/2012
 * Time: 13:17
 */
public enum Role {

        authenticated, administrator, anonymous
    }

