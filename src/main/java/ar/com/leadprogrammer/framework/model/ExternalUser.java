package ar.com.leadprogrammer.framework.model;


import java.security.Principal;


public class ExternalUser implements Principal {

    private String id;

    private String firstName;

    private String lastName;

    private String emailAddress;

    private boolean isVerified;

    private String role;

    public ExternalUser() {}

    public ExternalUser(String userId) {
        this.id = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getId() {
        return id;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public String getName() {
        return emailAddress;
    }

    public String getRole() {
        return role;
    }
}
