package ar.com.leadprogrammer.framework.rest.autentication.exception;


/**
 * User: porter
 * Date: 13/03/2012
 * Time: 08:58
 */
public class AuthenticationException extends BaseWebApplicationException {

	private static final long serialVersionUID = 1620122832601867003L;

	public AuthenticationException() {
        super(401, "40102", "Authentication Error", "Authentication Error. The username or password were incorrect");
    }


}
