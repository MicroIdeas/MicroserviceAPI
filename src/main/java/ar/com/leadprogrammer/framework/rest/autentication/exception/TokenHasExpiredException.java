package ar.com.leadprogrammer.framework.rest.autentication.exception;


/**
 *
 * @version 1.0
 * @author: Iain Porter iain.porter@porterhead.com
 * @since 14/09/2012
 */
public class TokenHasExpiredException extends BaseWebApplicationException {

	private static final long serialVersionUID = -4025383440909427088L;

	public TokenHasExpiredException() {
        super(403, "40304", "Token has expired", "An attempt was made to load a token that has expired");
    }
}
