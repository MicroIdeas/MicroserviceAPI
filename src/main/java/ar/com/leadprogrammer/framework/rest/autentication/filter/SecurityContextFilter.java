package ar.com.leadprogrammer.framework.rest.autentication.filter;

import java.io.IOException;
import java.util.logging.Logger;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import org.springframework.beans.factory.annotation.Autowired;
import ar.com.leadprogrammer.framework.model.ExternalUser;
import ar.com.leadprogrammer.framework.rest.autentication.AuthorizationRequestContext;
import ar.com.leadprogrammer.framework.rest.autentication.AuthorizationService;
import ar.com.leadprogrammer.framework.rest.autentication.impl.SecurityContextImpl;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class SecurityContextFilter implements ContainerRequestFilter {
	protected static final String HEADER_AUTHORIZATION = "Authorization";
	protected static final String HEADER_DATE = "x-java-rest-date";
	protected static final String HEADER_NONCE = "nonce";

	private static final Logger logger = Logger
			.getLogger(SecurityContextFilter.class.getName());

	@Autowired
	private AuthorizationService authorizationService;

	public SecurityContextFilter( ) {
		
	}

	@Override
	public void filter(ContainerRequestContext requestContext)
			throws IOException {

		logger.info("Entering JAX-RS Container Filter");

			requestContext.getHeaders();

			String authToken = requestContext
					.getHeaderString(HEADER_AUTHORIZATION);
			String requestDateString = requestContext
					.getHeaderString(HEADER_DATE);
			String nonce = requestContext.getHeaderString(HEADER_NONCE);
			AuthorizationRequestContext context = new AuthorizationRequestContext(
					requestContext.getUriInfo().getPath(),
					requestContext.getMethod(), requestDateString, nonce,
					authToken);
			ExternalUser externalUser = authorizationService.authorize(context);
			requestContext.setSecurityContext(new SecurityContextImpl(
					externalUser));

	}

}