package ar.com.leadprogrammer.framework.rest.autentication.exception;


/**
 *
 * @version 1.0
 * @author: Iain Porter iain.porter@porterhead.com
 * @since 14/09/2012
 */
public class TokenNotFoundException extends BaseWebApplicationException {

	private static final long serialVersionUID = -8027738344389859927L;

	public TokenNotFoundException() {
        super(404, "40407", "Token Not Found", "No token could be found for that Id");
    }
}
